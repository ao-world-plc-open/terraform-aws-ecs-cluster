# Change Log

## v4.1.0 (08/12/2021)

* Added .terraform-doc.yml to auto generate (mostly) README.md
* Added basic example config for README.md
* All variables and outputs have descriptions and types
* Ran `terraform fmt` on the repository
* Updated CONTRIBUTING.md to cover running fmt and ensuring docs are
  regenerated
* Added .gitlab-ci.yml to do basic checks for fmt and out of date docs

## v4.0.0 (28/05/2021)

* Added minimum provider version constraints per Terraform best practice
* Confirmed terraform validate passing for terraform 0.13, 0.14, and 0.15
* Minimum aws provider version set to where support for .aws/sso/cache is enabled
* Terraform 0.13 now the minimum version due to use of required_providers syntax

## v3.0.0 (04/02/2020)

- BREAKING CHANGE - removed the variable for the secondary capacity provider name and changed some resource naming to use the cluster name variable instead.

## v2.0.5 (04/11/2020)

- Remove trailing commas from user_data

## v2.0.4 (21/09/2020)

- Fix type and conditionals compatibility issues.

## v2.0.3 (21/09/2020)

- Fix type compatibility issues.

## v2.0.2 (21/09/2020)

- Fix type compatibility issues.

## v2.0.1 (21/09/2020)

- Upgrade ASG module dependency to terraform v12.

## v2.0.0 (21/09/2020)

- Upgrade the module for terraform v12.

## v1.1.1 (12/08/2020)

- Adds cluster ID and ARN as outputs from the module.

## v1.1.0 (11/08/2020)

- Adds the capability to include 2 capacity providers. This can be either a Windows or linux based AMI.
- Included some extra infrastructure to allow for the extra capacity provider. Launch template, ASG.

## v1.0.0 BREAKING CHANGE (06/06/2020)

- Replaced usage of an ASG module from the Terraform Registry to on hosted on Gitlab. Due to naming changes in the modules the ASG will be deleted and re-created. To prevent an outage you will need to detach the current ECS instances from the Auto Scaling Group, run `Terraform Apply`, allow the new instances to join the ECS cluster, drain the old instances and finally terminate them.
- Adds new argument custom_instance_tags - these tags will be appended to the tags applied by the `tags` variable
- Adds new argument custom_volume_tags - these tags will be appended to the tags applied by the `tags` variable

## v0.8.0 (12/05/2020)

- Fixes issue where the module will throw an error because it is trying to create a policy for logging even if logging is set to false.

## v0.7.0 (11/05/2020)

- Adds CloudWatch logging for Windows ECS instances.

## v 0.6.0 (17/03/2020)

- Sets remote management IPs to be a variable.

## v 0.5.0 (30/10/2019)

- Removed/updated deprecated SSM policy for current.

## v 0.4.0 (06/09/2019)

- Fixed an issue surrounding variable use.

## v 0.3.0 (06/09/2019)

- Fixes issue with ASG tags causing an error on apply.

## v 0.2.0

- Fixes issue with invalid variable.

## v 0.1.0

- Initial Commit.
