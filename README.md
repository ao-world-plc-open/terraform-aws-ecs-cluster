# Terraform Module: AWS ECS Cluster

This module will provision an AWS [ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)
Cluster and provision an auto scaling group with EC2 instances that will
register with the cluster. The module will also create a security group and
IAM role for the cluster. If desired the cluster can also be joined to an
Active Directory domain.

The module assumes you are passing an AMI ID of either the [AWS ECS Optimised image](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html)
or a customised image with the ECS agent/tools installed.

You will also need to provsion an [ALB](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html)
or [NLB](https://docs.aws.amazon.com/elasticloadbalancing/latest/network/introduction.html)
and Secuirty Group for load balancing purposes, this is not done by the
module.

* [Example Usage](#example-usage)
  * [Basic](#basic)
  * [Domain](#domain)
  * [Linux](#linux)
* [Requirements](#requirements)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [Contributing](#contributing)
* [Change Log](#change-log)

## Example Usage

### Basic

Deploy a basic ECS Cluster backed with an Auto Scaling Group.

```hcl
module "ecs_cluster" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-ecs-cluster.git?ref=v4.1.0"

  ami_name                    = "ami-05da69b2d804943e6"
  associate_public_ip_address = 0
  aws_alb_sg_id               = "sg-00a00000aa000a0a0a"
  desired_capacity            = 2
  domain_name                 = "contso.com"
  domain_name_servers_CIDR    = ["10.10.10.1/32", "10.10.11.1/32", "10.10.12.1/32", "10.10.13.1/32"]
  ecs_agent_version           = "v1.29.0"
  ecs_cluster_name            = "razor-qa-cluster"
  instance_type               = "t3.small"
  key_name                    = "access-keypair"
  max_size                    = 2
  min_size                    = 0
  remote_management_ips       = ["192.168.0.1", "10.10.10.1"]
  ssm_managed                 = 1
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
  team                = "razor"
  volume_type         = "gp2"
  vpc_id              = "vpc-00aaa0a00000000a"
  vpc_zone_identifier = ["subnet-000a00000000000", "subnet-111b11111111111"]
}
```

### Domain

Deploy an ECS Cluster backed with an Auto Scaling Group with instances joined
to an Active Directory domain.

```hcl
module "ad_ecs_cluster" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-ecs-cluster.git?ref=v4.1.0"

  account_ou                     = "DC=contso,DC=com"
  ami_name                       = "ami-05da69b2d804943e6"
  associate_public_ip_address    = 0
  aws_alb_sg_id                  = "sg-00a00000aa000a0a0a"
  desired_capacity               = 2
  domain_name                    = "contso.com"
  domain_name_servers_CIDR       = ["10.10.10.1/32", "10.10.11.1/32", "10.10.12.1/32", "10.10.13.1/32"]
  ecs_cluster_name               = "razor-qa-cluster"
  ecs_agent_version              = "v1.29.0"
  instance_type                  = "t3.small"
  join_domain                    = "1"
  key_name                       = "acccess-keypair"
  max_size                       = 2
  min_size                       = 0
  remote_management_ips          = ["192.168.0.1", "10.10.10.1"]
  ssm_managed                    = 1
  ssm_param_domain_join_password = "domain-join-password"
  ssm_param_domain_join_user     = "domain-join-user"
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
  team                = "razor"
  volume_type         = "gp2"
  vpc_id              = "vpc-00aaa0a00000000a"
  vpc_zone_identifier = ["subnet-000a00000000000", "subnet-111b11111111111"]
}
```

### Linux
Deploy an ECS Cluster back with an Auto Scaling Group of Linux instances.

```hcl
module "ad_ecs_cluster" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-ecs-cluster.git?ref=v4.1.0"

  ami_name                    = "ami-05da69b2d804943e6"
  associate_public_ip_address = 0
  aws_alb_sg_id               = "sg-00a00000aa000a0a0a"
  desired_capacity            = 2
  domain_name                 = "contso.com"
  domain_name_servers_CIDR    = ["10.10.10.1/32", "10.10.11.1/32", "10.10.12.1/32", "10.10.13.1/32"]
  ecs_cluster_name            = "razor-qa-cluster"
  isLinux                     = 1
  instance_type               = "t3.small"
  key_name                    = "acccess-keypair"
  max_size                    = 2
  min_size                    = 0
  remote_management_ips       = ["192.168.0.1", "10.10.10.1"]
  ssm_managed                 = 1
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
  team                = "razor"
  volume_type         = "gp2"
  vpc_id              = "vpc-00aaa0a00000000a"
  vpc_zone_identifier = ["subnet-000a00000000000", "subnet-111b11111111111"]
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 3.26.0 |
| null | >= 3.1.0 |
| template | >= 2.2.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| aws\_alb\_sg\_id | The Security Group ID of the ALB that front traffic for the ECS Instances | `string` | n/a | yes |
| domain\_name\_servers\_CIDR | DNS servers for the AD domain | `list(string)` | n/a | yes |
| ecs\_cluster\_name | The name of the ECS Cluster | `string` | n/a | yes |
| instance\_type | the EC2 Instance type that will be used for the cluster | `string` | n/a | yes |
| key\_name | the name of the EC2 Key Pair used to retrieve the administrator password through the AWS API | `string` | n/a | yes |
| volume\_type | Set the volume type for the container instances | `string` | n/a | yes |
| vpc\_id | The ID of the VPC the cluster will be placed in | `string` | n/a | yes |
| vpc\_zone\_identifier | The subnet ID's where the EC2 cluster nodes will be placed | `list(string)` | n/a | yes |
| account\_ou | The name of the OU that the computer account will be created in. Defined in RFC1779 format | `string` | `"DC=contso,DC=com"` | no |
| ami\_name | The AMI to use for the EC2 Cluster Nodes | `string` | `"amzn2-ami-ecs-hvm"` | no |
| associate\_public\_ip\_address | If enabled the EC2 instances will recieve public IP addresses | `string` | `0` | no |
| capacity\_provider | If this is active it will create a capacity provider along with the asg from the asg module | `number` | `0` | no |
| capacity\_providers | This is a list of capacity providers that must be listed when calling the ECS module | `list(string)` | `[]` | no |
| cloudwatch\_log\_retention\_days | The number of days to retain CloudWatch Logs - valid values are 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653. | `number` | `30` | no |
| cpu\_unbounded | Allows tasks set to 0 cpu units to be placed on a container instance | `string` | `"0"` | no |
| custom\_instance\_tags | A map of tags to apply to the ECS cluster instances, these will be merged with tags provided in the tags variable | `map(string)` | `{}` | no |
| custom\_volume\_tags | A map of tags to apply to the ECS cluster instance volumes, these will be merged with tags provided in the tags variable | `map(string)` | `{}` | no |
| desired\_capacity | The desired capacity of the ASG | `string` | `2` | no |
| domain\_name | The name of the AD Domain to join | `string` | `""` | no |
| ebs\_optimized | Turns on EBS optimisation | `string` | `"true"` | no |
| ecs\_agent\_version | The version of the ECS agent to deploy | `string` | `"v1.26.0"` | no |
| ecs\_instance\_logs\_to\_cloudwatch | If enabled the ECS Instances will ship ECS Agent logs to CloudWatch | `bool` | `false` | no |
| ecs\_log\_level | The level of debugging for ecs logging | `string` | `"info"` | no |
| envtype | the type of environment the cluster will be deployed in | `string` | `"staging"` | no |
| health\_check\_type | The type of healthcheck for the ASG | `string` | `"EC2"` | no |
| isLinux | If enabled then depicts that the ECS cluster is using Linux | `string` | `"0"` | no |
| join\_domain | Set if the the cluster is part of the Active Directory Domain. | `string` | `"0"` | no |
| max\_size | the maximum size of the ASG | `string` | `2` | no |
| min\_size | The minimum size of the ASG | `string` | `0` | no |
| owners | List of AMI owners to limit search. At least 1 value must be specified. Valid values: an AWS account ID, self (the current account), or an AWS owner alias (e.g. amazon, aws-marketplace, microsoft). | `list(string)` | ```[ "amazon" ]``` | no |
| region | The region that will be set as the shell default on the instance | `string` | `"eu-west-1"` | no |
| remote\_managament\_ips | A list of IP's to grant remote management access to | `list(string)` | ```[ "10.10.60.0/23", "10.10.70.0/23", "10.250.248.0/21", "109.111.215.168/32" ]``` | no |
| root\_volume\_size | Sets the root volume size for the containers instances | `string` | `"200"` | no |
| second\_capacity\_provider | If this is active it will create a capacity prover along with the second asg | `number` | `0` | no |
| second\_capacity\_provider\_ami | The AMI to use for the second capacity provider cluster Nodes | `string` | `"amzn2-ami-ecs-hvm"` | no |
| spot\_price | If set the ASG will use spot instances at the set bid price, if not on demand instances are used | `string` | `""` | no |
| ssm\_managed | If enabled the SSM managed policy will be applied to the cluster nodes | `string` | `"0"` | no |
| ssm\_param\_domain\_join\_password | The name of the SSM parameter that contains the password that wil be used to authenticate the domain join | `string` | `""` | no |
| ssm\_param\_domain\_join\_user | The name of the SSM parameter that contains the username that will be used to authenticate the domain join | `string` | `""` | no |
| subnets | These are the subnets that will be used by the instances spawned from the second asg/launch template | `list(string)` | `[]` | no |
| tags | A map of tags to add to all resources | `map(string)` | `{}` | no |
| target\_group\_arns | These are the target group arns | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| cluster\_security\_group\_id | *string* The ID of the ASG instances Security Group |
| ecs\_cluster\_arn | *string* The ARN of the ECS Cluster |
| ecs\_cluster\_asg\_name | *string* The name of the ASG assigned to the Cluster |
| ecs\_cluster\_id | *string* The ID of the ECS Cluster |

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

## Change Log

Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.