module "ad_ecs_cluster" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-ecs-cluster.git?ref=v4.1.0"

  ami_name                    = "ami-05da69b2d804943e6"
  associate_public_ip_address = 0
  aws_alb_sg_id               = "sg-00a00000aa000a0a0a"
  desired_capacity            = 2
  domain_name                 = "contso.com"
  domain_name_servers_CIDR    = ["10.10.10.1/32", "10.10.11.1/32", "10.10.12.1/32", "10.10.13.1/32"]
  ecs_cluster_name            = "razor-qa-cluster"
  isLinux                     = 1
  instance_type               = "t3.small"
  key_name                    = "acccess-keypair"
  max_size                    = 2
  min_size                    = 0
  remote_management_ips       = ["192.168.0.1", "10.10.10.1"]
  ssm_managed                 = 1
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
  team                = "razor"
  volume_type         = "gp2"
  vpc_id              = "vpc-00aaa0a00000000a"
  vpc_zone_identifier = ["subnet-000a00000000000", "subnet-111b11111111111"]
}