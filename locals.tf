locals {
  tags_asg_format = null_resource.tags_as_list_of_maps.*.triggers
}

resource "null_resource" "tags_as_list_of_maps" {
  count = length(keys(var.custom_instance_tags))

  triggers = {
    "key"                 = element(keys(var.custom_instance_tags), count.index)
    "value"               = element(values(var.custom_instance_tags), count.index)
    "propagate_at_launch" = "false"
  }
}

