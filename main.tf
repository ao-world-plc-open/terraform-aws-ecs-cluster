resource "aws_ecs_cluster" "cluster" {
  depends_on = [
    aws_ecs_capacity_provider.capacity_provider_secondary,
    aws_ecs_capacity_provider.capacity_provider,
  ]
  name               = var.ecs_cluster_name
  capacity_providers = var.capacity_providers
}

module "ecs_cluster_asg" {
  source        = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-asg.git?ref=v4.0.1"
  name          = "${var.ecs_cluster_name}-asg"
  envname       = var.envtype
  subnets       = var.vpc_zone_identifier
  ami_owners    = var.owners
  ami_id        = var.ami_name
  instance_type = var.instance_type
  key_name      = var.key_name
  security_groups = split(
    ",",
    var.join_domain != "0" ? join(
      ",",
      flatten(
        [
          aws_security_group.ecsInstance-ecs.*.id,
          aws_security_group.ads_ec2_on_prem_sg.*.id,
        ],
      ),
    ) : aws_security_group.ecsInstance-ecs.id,
  )

  user_data = var.join_domain == "1" ? format("<powershell>%s%s</powershell>", data.template_file.domain_connect.rendered, data.template_file.ecs.rendered) : format(var.isLinux == "0" ? "<powershell>%s</powershell>" : "%s", data.template_file.ecs.rendered)

  associate_public_ip_address = var.associate_public_ip_address
  detailed_monitoring         = "true"
  asg_min                     = var.min_size
  asg_max                     = var.max_size
  asg_desired                 = var.desired_capacity
  health_check_type           = var.health_check_type
  health_check_grace_period   = 1500
  protect_from_scale_in       = var.capacity_provider
  custom_instance_tags        = merge(var.tags, var.custom_instance_tags)
  custom_volume_tags          = merge(var.tags, var.custom_volume_tags)
  iam_instance_profile        = aws_iam_instance_profile.ecsInstance.id
}

resource "aws_ecs_capacity_provider" "capacity_provider" {
  count = var.capacity_provider
  name  = "${module.ecs_cluster_asg.asg_name}-capacity-provider"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = module.ecs_cluster_asg.asg_arn
    managed_termination_protection = "ENABLED"

    managed_scaling {
      maximum_scaling_step_size = 1000
      minimum_scaling_step_size = 1
      status                    = "ENABLED"
      target_capacity           = 10
    }
  }
}

resource "aws_ecs_capacity_provider" "capacity_provider_secondary" {
  count = var.second_capacity_provider
  name  = "${var.ecs_cluster_name}-secondary-capacity-provider"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.second_ecs_cluster_asg[0].arn
    managed_termination_protection = "ENABLED"

    managed_scaling {
      maximum_scaling_step_size = 1000
      minimum_scaling_step_size = 1
      status                    = "ENABLED"
      target_capacity           = 10
    }
  }
}

resource "aws_autoscaling_group" "second_ecs_cluster_asg" {
  count = var.second_capacity_provider
  name  = "${var.ecs_cluster_name}-secondary-capacity-provider-asg"
  # TF-UPGRADE-TODO: In Terraform v0.10 and earlier, it was sometimes necessary to
  # force an interpolation expression to be interpreted as a list by wrapping it
  # in an extra set of list brackets. That form was supported for compatibility in
  # v0.11, but is no longer supported in Terraform v0.12.
  #
  # If the expression in the following list itself returns a list, remove the
  # brackets to avoid interpretation as a list of lists. If the expression
  # returns a single list item then leave it as-is and remove this TODO comment.
  vpc_zone_identifier = module.ecs_cluster_asg.subnets

  launch_template {
    id      = aws_launch_template.second_capacity_provider_launch_template[0].id
    version = "$Latest"
  }

  target_group_arns         = var.target_group_arns
  min_size                  = var.min_size
  max_size                  = var.max_size
  desired_capacity          = var.desired_capacity
  wait_for_capacity_timeout = module.ecs_cluster_asg.wait_for_capacity_timeout
  health_check_grace_period = module.ecs_cluster_asg.health_check_grace_period
  health_check_type         = var.health_check_type
  protect_from_scale_in     = var.second_capacity_provider == 0 ? false : true

  # TF-UPGRADE-TODO: In Terraform v0.10 and earlier, it was sometimes necessary to
  # force an interpolation expression to be interpreted as a list by wrapping it
  # in an extra set of list brackets. That form was supported for compatibility in
  # v0.11, but is no longer supported in Terraform v0.12.
  #
  # If the expression in the following list itself returns a list, remove the
  # brackets to avoid interpretation as a list of lists. If the expression
  # returns a single list item then leave it as-is and remove this TODO comment.
  tags = concat(
    [
      {
        "key"                 = "Name"
        "value"               = "${var.ecs_cluster_name}-secondary-capacity-provider-asg"
        "propagate_at_launch" = false
      },
    ],
    [
      {
        "key"                 = "Environment"
        "value"               = var.envtype
        "propagate_at_launch" = false
      },
    ],
    local.tags_asg_format,
  )
}

resource "aws_launch_template" "second_capacity_provider_launch_template" {
  count         = var.second_capacity_provider
  ebs_optimized = var.ebs_optimized

  iam_instance_profile {
    name = aws_iam_instance_profile.ecsInstance.id
  }

  image_id                             = data.aws_ami.ecs_optimised_second_cp[0].id
  instance_initiated_shutdown_behavior = "terminate"
  instance_type                        = var.instance_type
  key_name                             = var.key_name

  monitoring {
    enabled = "true"
  }

  network_interfaces {
    delete_on_termination       = true
    associate_public_ip_address = var.associate_public_ip_address
    security_groups = split(
      ",",
      var.join_domain != "0" ? join(
        ",",
        flatten(
          [
            aws_security_group.ecsInstance-ecs.*.id,
            aws_security_group.ads_ec2_on_prem_sg.*.id,
          ],
        ),
      ) : aws_security_group.ecsInstance-ecs.id,
    )
  }

  tag_specifications {
    resource_type = "instance"

    tags = merge(
      {
        "Name" = "${var.ecs_cluster_name}-secondary-capacity-provider-asg"
      },
      {
        "Environment" = var.envtype
      },
      var.custom_instance_tags,
      var.tags,
    )
  }

  user_data = base64encode(data.template_file.ecs-linux.rendered)

  tag_specifications {
    resource_type = "volume"

    tags = merge(
      {
        "Name" = "${var.ecs_cluster_name}-secondary-capacity-instance-volume"
      },
      {
        "Environment" = var.envtype
      },
      var.custom_volume_tags,
      var.tags,
    )
  }
}

resource "aws_security_group" "ecsInstance-ecs" {
  name   = "${var.ecs_cluster_name} instance security group"
  vpc_id = var.vpc_id

  tags = merge(
    {
      "Name" = "${var.ecs_cluster_name}-instance-sg"
    },
    var.tags,
  )
}

resource "aws_security_group_rule" "ecsInstance-ecs-80-egress" {
  protocol          = "TCP"
  type              = "egress"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecsInstance-ecs.id
}

resource "aws_security_group_rule" "ecsInstance-ecs-443-egress" {
  protocol          = "TCP"
  type              = "egress"
  from_port         = 443
  to_port           = 443
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecsInstance-ecs.id
}

resource "aws_security_group_rule" "ecs-all-ingress" {
  protocol                 = "TCP"
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  security_group_id        = aws_security_group.ecsInstance-ecs.id
  source_security_group_id = var.aws_alb_sg_id
}

resource "aws_security_group_rule" "devopscluster_instances_out" {
  type                     = "egress"
  protocol                 = "tcp"
  from_port                = 0
  to_port                  = 65535
  security_group_id        = var.aws_alb_sg_id
  source_security_group_id = aws_security_group.ecsInstance-ecs.id
}

resource "aws_security_group_rule" "ecsInstance-rdp-ingress" {
  protocol          = "TCP"
  type              = "ingress"
  from_port         = 3389
  to_port           = 3389
  security_group_id = aws_security_group.ecsInstance-ecs.id
  cidr_blocks       = var.remote_managament_ips
}

resource "aws_security_group_rule" "ecsInstance_dns_tcp" {
  type              = "egress"
  from_port         = 53
  to_port           = 53
  protocol          = "tcp"
  cidr_blocks       = var.domain_name_servers_CIDR
  security_group_id = aws_security_group.ecsInstance-ecs.id
  description       = "allow tcp 53 outbound to DNS servers"
}

resource "aws_security_group_rule" "ecsInstance_dns_udp" {
  type              = "egress"
  from_port         = 53
  to_port           = 53
  protocol          = "udp"
  cidr_blocks       = var.domain_name_servers_CIDR
  security_group_id = aws_security_group.ecsInstance-ecs.id
  description       = "Allow UDP 53 outbound to DNS servers"
}

resource "aws_security_group" "ads_ec2_on_prem_sg" {
  count       = var.join_domain
  name        = "${var.ecs_cluster_name} ads-ec2-on-prem"
  vpc_id      = var.vpc_id
  description = "${var.ecs_cluster_name} AD security group"
}

resource "aws_security_group_rule" "egress-1024-65535-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "TCP"
  from_port         = "49152"
  to_port           = "65535"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-1024-65535-UDP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "UDP"
  from_port         = "49152"
  to_port           = "65535"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-123-123-UDP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "UDP"
  from_port         = "123"
  to_port           = "123"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-135-135-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "TCP"
  from_port         = "135"
  to_port           = "135"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-389-389-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "TCP"
  from_port         = "389"
  to_port           = "389"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-389-389-UDP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "UDP"
  from_port         = "389"
  to_port           = "389"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-445-445-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "TCP"
  from_port         = "445"
  to_port           = "445"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-53-53-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "TCP"
  from_port         = "53"
  to_port           = "53"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-53-53-UDP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "UDP"
  from_port         = "53"
  to_port           = "53"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-88-88-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "TCP"
  from_port         = "88"
  to_port           = "88"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "egress-88-88-UDP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "egress"
  protocol          = "UDP"
  from_port         = "88"
  to_port           = "88"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "ingress-1024-65535-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "49152"
  to_port           = "65535"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "ingress-123-123-UDP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "ingress"
  protocol          = "UDP"
  from_port         = "123"
  to_port           = "123"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "ingress-135-135-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "135"
  to_port           = "135"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "ingress-139-139-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "139"
  to_port           = "139"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "ingress-445-445-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "445"
  to_port           = "445"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "ingress-88-88-TCP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "88"
  to_port           = "88"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_security_group_rule" "ingress-88-88-UDP" {
  count             = var.join_domain != "0" ? 1 : 0
  type              = "ingress"
  protocol          = "UDP"
  from_port         = "88"
  to_port           = "88"
  security_group_id = aws_security_group.ads_ec2_on_prem_sg[0].id
  cidr_blocks       = var.domain_name_servers_CIDR
}

resource "aws_iam_instance_profile" "ecsInstance" {
  name = aws_iam_role.ecsInstance.name
  role = aws_iam_role.ecsInstance.name
  path = "/"
}

resource "aws_iam_role" "ecsInstance" {
  name = "${var.envtype}-${var.ecs_cluster_name}-ecsInstanceRole"

  assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY


  tags = var.tags
}

data "aws_iam_policy" "ssm_interaction_policy" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

data "aws_iam_policy" "AmazonEC2ContainerServiceforEC2Role" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "ecsInstance" {
  policy_arn = data.aws_iam_policy.AmazonEC2ContainerServiceforEC2Role.arn
  role       = aws_iam_role.ecsInstance.name
}

resource "aws_iam_role_policy_attachment" "ssm_policy" {
  policy_arn = data.aws_iam_policy.ssm_interaction_policy.arn
  role       = aws_iam_role.ecsInstance.name
  count      = var.ssm_managed
}

resource "aws_iam_role_policy" "join_domain" {
  role  = aws_iam_role.ecsInstance.name
  name  = "join_domain"
  count = var.join_domain == 0 ? 0 : 1

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "ssm:GetParameter",
            "Resource": [
                "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/${var.ssm_param_domain_join_password}",
                "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/${var.ssm_param_domain_join_user}"
            ]
        }
    ]
}
EOF

}

