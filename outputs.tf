output "cluster_security_group_id" {
  description = "*string* The ID of the ASG instances Security Group"
  value       = aws_security_group.ecsInstance-ecs.id
}

output "ecs_cluster_arn" {
  description = "*string* The ARN of the ECS Cluster"
  value       = aws_ecs_cluster.cluster.arn
}

output "ecs_cluster_asg_name" {
  description = "*string* The name of the ASG assigned to the Cluster"
  value       = module.ecs_cluster_asg.asg_name
}

output "ecs_cluster_id" {
  description = "*string* The ID of the ECS Cluster"
  value       = aws_ecs_cluster.cluster.id
}
