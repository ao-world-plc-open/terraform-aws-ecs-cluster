variable "account_ou" {
  description = "The name of the OU that the computer account will be created in. Defined in RFC1779 format"
  type        = string
  default     = "DC=contso,DC=com"
}

variable "ami_name" {
  description = "The AMI to use for the EC2 Cluster Nodes"
  type        = string
  default     = "amzn2-ami-ecs-hvm"
}

variable "associate_public_ip_address" {
  description = "If enabled the EC2 instances will recieve public IP addresses"
  type        = string
  default     = 0
}

variable "aws_alb_sg_id" {
  description = "The Security Group ID of the ALB that front traffic for the ECS Instances"
  type        = string
}

variable "capacity_provider" {
  description = "If this is active it will create a capacity provider along with the asg from the asg module"
  type        = number
  default     = 0
}

variable "capacity_providers" {
  description = "This is a list of capacity providers that must be listed when calling the ECS module"
  type        = list(string)
  default     = []
}

variable "cpu_unbounded" {
  description = "Allows tasks set to 0 cpu units to be placed on a container instance"
  type        = string
  default     = "0"
}

variable "cloudwatch_log_retention_days" {
  description = "The number of days to retain CloudWatch Logs - valid values are 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653."
  type        = number
  default     = 30
}

variable "custom_volume_tags" {
  description = "A map of tags to apply to the ECS cluster instance volumes, these will be merged with tags provided in the tags variable"
  type        = map(string)
  default     = {}
}

variable "custom_instance_tags" {
  description = "A map of tags to apply to the ECS cluster instances, these will be merged with tags provided in the tags variable"
  type        = map(string)
  default     = {}
}

variable "desired_capacity" {
  description = "The desired capacity of the ASG"
  type        = string
  default     = 2
}

variable "domain_name" {
  description = "The name of the AD Domain to join"
  type        = string
  default     = ""
}

variable "domain_name_servers_CIDR" {
  description = "DNS servers for the AD domain"
  type        = list(string)
}

variable "ebs_optimized" {
  description = "Turns on EBS optimisation"
  type        = string
  default     = "true"
}

variable "ecs_agent_version" {
  description = "The version of the ECS agent to deploy"
  type        = string
  default     = "v1.26.0"
}

variable "ecs_cluster_name" {
  description = "The name of the ECS Cluster"
  type        = string
}

variable "ecs_instance_logs_to_cloudwatch" {
  description = "If enabled the ECS Instances will ship ECS Agent logs to CloudWatch"
  default     = false
}

variable "ecs_log_level" {
  description = "The level of debugging for ecs logging"
  type        = string
  default     = "info"
}

variable "envtype" {
  description = "the type of environment the cluster will be deployed in"
  type        = string
  default     = "staging"
}

variable "health_check_type" {
  description = "The type of healthcheck for the ASG"
  type        = string
  default     = "EC2"
}

variable "instance_type" {
  description = "the EC2 Instance type that will be used for the cluster"
  type        = string
}

variable "isLinux" {
  description = "If enabled then depicts that the ECS cluster is using Linux"
  type        = string
  default     = "0"
}

variable "join_domain" {
  description = "Set if the the cluster is part of the Active Directory Domain."
  type        = string
  default     = "0"
}

variable "key_name" {
  description = "the name of the EC2 Key Pair used to retrieve the administrator password through the AWS API"
  type        = string
}

variable "max_size" {
  description = "the maximum size of the ASG"
  type        = string
  default     = 2
}

variable "min_size" {
  description = "The minimum size of the ASG"
  type        = string
  default     = 0
}

variable "owners" {
  description = "List of AMI owners to limit search. At least 1 value must be specified. Valid values: an AWS account ID, self (the current account), or an AWS owner alias (e.g. amazon, aws-marketplace, microsoft)."
  type        = list(string)
  default     = ["amazon"]
}

variable "region" {
  description = "The region that will be set as the shell default on the instance"
  type        = string
  default     = "eu-west-1"
}

variable "remote_managament_ips" {
  description = "A list of IP's to grant remote management access to"
  type        = list(string)
  default     = ["10.10.60.0/23", "10.10.70.0/23", "10.250.248.0/21", "109.111.215.168/32"]
}

variable "root_volume_size" {
  description = "Sets the root volume size for the containers instances"
  type        = string
  default     = "200"
}

variable "second_capacity_provider" {
  description = "If this is active it will create a capacity prover along with the second asg"
  type        = number
  default     = 0
}

variable "second_capacity_provider_ami" {
  description = "The AMI to use for the second capacity provider cluster Nodes"
  type        = string
  default     = "amzn2-ami-ecs-hvm"
}

variable "spot_price" {
  description = "If set the ASG will use spot instances at the set bid price, if not on demand instances are used"
  type        = string
  default     = ""
}

variable "ssm_managed" {
  type        = string
  default     = "0"
  description = "If enabled the SSM managed policy will be applied to the cluster nodes"
}

variable "ssm_param_domain_join_password" {
  description = "The name of the SSM parameter that contains the password that wil be used to authenticate the domain join"
  type        = string
  default     = ""
}

variable "ssm_param_domain_join_user" {
  description = "The name of the SSM parameter that contains the username that will be used to authenticate the domain join"
  type        = string
  default     = ""
}

variable "subnets" {
  description = "These are the subnets that will be used by the instances spawned from the second asg/launch template"
  type        = list(string)
  default     = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "target_group_arns" {
  description = "These are the target group arns"
  type        = list(string)
  default     = []
}

variable "volume_type" {
  description = "Set the volume type for the container instances"
  type        = string
}

variable "vpc_id" {
  description = "The ID of the VPC the cluster will be placed in"
  type        = string
}

variable "vpc_zone_identifier" {
  description = "The subnet ID's where the EC2 cluster nodes will be placed"
  type        = list(string)
}
